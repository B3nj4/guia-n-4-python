from tabulate import tabulate

def crea_dic():
    dic = { "7 BQL" : "The crystal structure of PdxI complex with the Alder - ene adduct" ,
            "7 BQJ" : "The structure of PdxI",
            "7 BVZ" : "Crystal structure of MreB5 of Spiroplasma citri bound to ADP" } 
    return dic

def imprime_dic(dic):
    cabecera = ["Proteinas","Descripcion"]
    #print(tabulate(dic,headers= cabecera))
    #print(tabulate(dic, headers='cabecera', tablefmt='grid'))
    #print(tabulate(dic, headers= "keys", showindex= "value"))
    #print(tabulate(dic))
    print(tabulate(dic.items(), headers = cabecera))
    return dic

def agrega_elementos(dic):
    
    crea_dic()
    a_key = input("Ingrese el nombre de la proteina que desee agregar ---> ")
    a_value = input("Ingrese una breve descripcion de dicha proteina ---> ")
    dic[a_key] = a_value
    return dic   

def editar_elementos(dic):

    crea_dic()
    edi_key = input("Ingrese nombre de proteina que desee editar ---> ")
    
    try:
        print("Si quieres modificar <",edi_key,"> es pertinente mostrarte su descripcion: <" ,dic[edi_key],">") 
        edi_value = input("Ingrese la modificacion que desee ---> ")
        dic[edi_key] = edi_value
    except:
        print("<<<< Nombre no congruente >>>>")
        pass
    
    return dic

def consultar_elementos(dic):

    crea_dic()
    con_key = input("\nIngrese el nombre de la proteina que quiera consultar ---> ")
    
    try:
        print("Nombre de la proteina: < ",con_key," > ; descripcion: < ",dic[con_key]," >")
    except:
        print("<<<< Nombre no congruente >>>>")

    return dic

def eliminar_elemento(dic):

    crea_dic()
    dele_key = input("Ingrese nombre de proteina a borrar ---> ")
        
    try:
        del dic[dele_key]
            
    except:
        print("<<<< Nombre no congruente o no existe >>>>")
        pass

    return dic    
