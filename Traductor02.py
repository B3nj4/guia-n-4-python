from tabulate import tabulate

def crea_repertorio():
    repertorio = {}
    return repertorio

crea_repertorio()

def imprime_repertorio(repertorio):
    
    crea_repertorio()
    cabecera = ["Espagnol","English"]
    print(tabulate(repertorio.items(), headers = cabecera))
    
    return repertorio

def agrega_elementos(repertorio):
    
    crea_repertorio()
    frase = input("Ingrese una frase en espagnol ---> ")
    lista = frase.split(" ")

    for elemento in lista:
        repertorio[elemento] = " "   
        print("¿Sabe la traduccion de la palabra < ",elemento," > ? (S/n)")
        sabe = input()
    
        if sabe == "S":
            traduccion = input("Traduscala con confianza --> ")
            repertorio[elemento] = traduccion
        elif sabe == "n":
            pass
        else:
            print("Ingrese (S) si la respuesta es afirmativa, en su defecto, ingrese (n) para una respuesta negativa. La palabra no se analizará esta vez\n ")
            pass
    
    return repertorio

def consulta_palabra(repertorio):

    crea_repertorio()
    con_key = input("\nPalabra que quiere consultar ---> ")
    
    try:
        print("Palabra en español: < ",con_key," > ; traducida: < ",repertorio[con_key]," >")
    except:
        print("<<<< Palabra no congruente >>>>")
    
    return repertorio
