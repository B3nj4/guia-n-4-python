Guia Nº4 - Python 

"Hola Mundo"

<<<<< Respecto al primer programa: "Humilde base de datos proteica" >>>>>

¿Cuales son los requisitos?
Para correr el programa se requiere de los ficheros "Main01.py" y "CreaDic.py" ademas de un IDE o editor de texto adecuado ... y porsupuesto Python 3.8 

¿De que trata el programa?
El programa trata emular una "base de datas" con informacion referente a proteinas, adjuntando los nombres y descripciones breves de estas. Lo interesante del programa es que requiere de una interaccion con el usuario el cual podra no solo agregar y consultar la base de datas, si no que ademas, podra eliminar e inclusive editar las proteinas que desee.

¿Como interactuar con el programa?
Simplemente atenerse a las ordenes generadas por el "Menu" del programa que saldra apenas se ejecute el programa

¿Referencias?
Para hacer la tabla utilice este link: "https://living-sun.com/es/python/685485-python-tabulate-doesn39t-produce-the-correct-table-from-dictionary-python-dictionary.html" y este "https://python-para-impacientes.blogspot.com/2017/01/tablas-con-estilo-con-tabulate.html".

¿Observaciones?
Al tener este programa el uso de librerias externas "tabulate" pueden haber discrepancias en el programa.
Para conseguirlo utilice este sitio web "https://howtoinstall.co/es/ubuntu/xenial/python3-tabulate" (hay que tener ojo con la version, ya que, el programa es a fin con la versio 3 de Python, otras versiones mararan error).


<<<<< Respecto al segundo programa: "Humilde traductor " >>>>>

¿Cuales son los requisitos?
Se requieren el fichero principal "Menu02.py" y el las funciones secundarias a este:"Traductor02.py".
Se requiere de un IDE o editor de texto adecuado amen de tener Python 3.8 marchando

¿De que trata el programa?
Como bien nos hace saber el titulo del programa, se intenta emular un traductor palabra por palabra. Tiene una naturaleza interactiva lo cual la propia persona puede ir escribiendo; por cada palabraen español, su traduccion en ingles, sin embargo, en caso de no saber la traduccion podra saltarsela quedando el nombre de la palabra en español guardada para que en un futuro se puede agregar la respectiva traduccion.

¿Como interactuar con el programa?
Simplemente atenerse a las ordenes generadas por el "Menu" del programa que saldra apenas se ejecute el programa

¿Observaciones?
Al tener este programa el uso de librerias externas "tabulate" pueden haber discrepancias en el programa.
Para conseguirlo utilice este sitio web "https://howtoinstall.co/es/ubuntu/xenial/python3-tabulate" (hay que tener ojo con la version, ya que, el programa es a fin con la versio 3 de Python, otras versiones mararan error)


Por Benjamin Vera Garrido

Gracias


 
