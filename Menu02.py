import Traductor02
from tabulate import tabulate

def menu():
    
    menu = [" "] * 5
    menu[0] = "--------------------------------------------------------------------"
    menu[1] = "   Presione(1) para ingresar una frase y su respectiva traduccion   "
    menu[2] = "   Presione(2) para consultar una palabra en espagnol               "
    menu[3] = "   Presione(3) para cerrar el programa                              "
    menu[4] = "--------------------------------------------------------------------"

    for fila in menu:
        texto = "< "
        texto = texto + fila + " "
        print(texto + " >") 
        
def intera_menu():
        
    repertorio = Traductor02.crea_repertorio()
    Traductor02.imprime_repertorio(repertorio)

    while True:
        print("\n")
        menu()
        tecla = input("\nPresione(numero) para elegir alguna opcion mostrada en el menú: ")
        
        if tecla == "1":
            Traductor02.agrega_elementos(repertorio)
            print("\n")
            Traductor02.imprime_repertorio(repertorio)
                                                        
        elif tecla == "2":
            Traductor02.consulta_palabra(repertorio)
            print("\n")
                               
        elif tecla == "3":
            print("\n\nArrivederci\n    ...\n")
            break

intera_menu()
