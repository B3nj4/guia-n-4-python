import CreaDic
from tabulate import tabulate

def menu():
	    
    mune = [" "] * 7
    mune[0] = "-----------------------------------------"
    mune[1] = "   Presione(1) para agregar proteina     "
    mune[2] = "   Presione(2) para editar proteina      "
    mune[3] = "   Presione(3) para consultar proteina   "
    mune[4] = "   Presione(4) para eliminar proteina    "
    mune[5] = "   Presione(5) para cerrar el programa   "
    mune[6] = "-----------------------------------------"
    
    for fila in mune:
        texto = "< "
        texto = texto + fila + " "
        print(texto + " >")        

def intera_menu():
    
    menu()
    print("\n")
    dic = CreaDic.crea_dic()
    CreaDic.imprime_dic(dic)
    
    while True:
        
        tecla = input("\nPresione(numero) para elegir alguna opcion mostrada en el menú: ")
                
        if tecla == "1":
            CreaDic.agrega_elementos(dic)
            print("\n")
            CreaDic.imprime_dic(dic)
                                            
        elif tecla == "2":
            CreaDic.editar_elementos(dic)
            print("\n")
            CreaDic.imprime_dic(dic)             

        elif tecla == "3":
            CreaDic.consultar_elementos(dic)
            #print("\n")
            #CreaDic.imprime_dic(dic)         
        
        elif tecla == "4":
            CreaDic.eliminar_elemento(dic)
            print("\n")
            CreaDic.imprime_dic(dic)             
        
        elif tecla == "5":
            print("\n\nArrivederci\n    ...\n")
            break

intera_menu()	
		 